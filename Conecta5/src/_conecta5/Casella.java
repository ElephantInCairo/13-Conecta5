/** Classe casella - José Sánchez Moreno - v.21.06.14.
 *
 * El constructor defineix una casella com un rectangle d'un color i si està
 * ocupada o no, el constructor no posa cap peça a la casella
 *
 * El mètode paintComponent pinta el rectangle de la casella si la casella està
 * ocupada pinta la pesa cridant al mètode de pintar peces
 *
 * El mètode setPesa col·loca una peça en la casella en questió
 */

package _conecta5;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;


/**
 *
 * @author jsm432 
 *  codi en base a la classe per miquelmascaro
 */

class Casella {
    
    private final Rectangle2D.Float rec;
    private Boolean ocupada;
    private Fitxa fitxa;

    //constructor---------------------------------------------------------
    public Casella(Rectangle2D.Float r) {
        this.rec = r;
        this.ocupada = false;
        this.fitxa = null;
    }

    //getters-setters-----------------------------------------------------
    public void setFitxa(Fitxa s) {
        this.ocupada = true;
        this.fitxa = s;
    }
    
    public Fitxa getFitxa(){
        return fitxa;
    }
    
    public boolean esPlena(){
        return ocupada;
    }
    
    public void buida(){
        this.fitxa = null;
        this.ocupada = false;
    }
    
    //altres--------------------------------------------------------------
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fill(this.rec);
        g2d.setColor(Color.WHITE);
        g2d.draw(rec);
        if (this.ocupada) {
            this.fitxa.paintComponent(g, this.rec.x, this.rec.y);
        }
    }
}
