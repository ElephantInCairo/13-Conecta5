/** Clase principal Conecta 5 - José Sánchez Moreno. */

//buidar tauler?

package _conecta5;

import java.awt.CardLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 *
 * @author jsm432
 * @since 18/06/2014
 */

public class Conecta5 extends JFrame {
    //static int bolasUsadas = 0;

    /**
     * variables de diseny
     */
    CardLayout cl = new CardLayout();
    JPanel contenedor, JPWelcome, JPPartida, JPPuntuaciones;
    JLabel JLJugar, JLTerminar;
    JLabel j1, j2, JLPausar, JLCancelar;
    Tauler tauler;

    /**
     * variables del joc
     */
    public static boolean pausado;
    public static boolean turno;
    public String jugador1 = "jugador1";
    public String jugador2 = "jugador2";
    
    
    /**
     * Constructor del JFrame
     */
    public Conecta5() {
        super("Conecta5");
        this.setSize(500,300);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initContents();
    }
    
    /**
     * inicialització del diseny
     */
    private void initContents() {
        
        //contenidor de tots els panells
        contenedor = new JPanel(cl);
        
        
        // Components gràfics de la pantalla de benvinguda----------------------
        JPWelcome = new JPanel();
        JLJugar = new JLabel("Jugar");
        JLTerminar = new JLabel("Terminar");
        
        //botó Jugar
        JLJugar.setFont(new Font("Nimbus Mono L", 0, 12));
        JLJugar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        JLJugar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                JLJugarMouseClicked(evt);
            }
        });
        JPWelcome.add(JLJugar);
        
        //botó Terminar
        JLTerminar.setFont(new Font("Nimbus Mono L", 0, 12));
        JLTerminar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        JLTerminar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                JLTerminarMouseClicked(evt);
            }
        });
        JPWelcome.add(JLTerminar);
        
        contenedor.add(JPWelcome, "bienvenida");
          
        
        // Components gràfics de la pantalla de joc-----------------------------
        JPPartida = new JPanel();
        JLPausar = new JLabel("Pausar");
        JLCancelar = new JLabel("Cancelar");
        tauler = new Tauler();
        
        //botó Pausar
        JLPausar.setFont(new Font("Nimbus Mono L", 0, 12));
        JLPausar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        JLPausar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                JLPausarMouseClicked(evt);
            }
        });
        JPPartida.add(JLPausar);
        
        //botó Cancelar
        JLCancelar.setFont(new Font("Nimbus Mono L", 0, 12));
        JLCancelar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        JLCancelar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                JLCancelarMouseClicked(evt);
            }

        });
        JPPartida.add(JLCancelar);
        
        //tauler de joc
        tauler.addMouseListener(tauler);
        JPPartida.add(tauler);

        contenedor.add(JPPartida, "juego");
        
        
        // Components gràfics de la pantalla de benvinguda---------------------- 
        JPPuntuaciones = new JPanel();
        
        //afegir l'area de text amb les dades del fitxer puntuacions
        
        contenedor.add(JPPuntuaciones, "puntuaciones");


        cl.show(contenedor, "bienvenida");
        this.getContentPane().add(contenedor);
        
    }
    
    private void JLPausarMouseClicked(MouseEvent evt) {
        
        if(pausado){
            
            tauler.addMouseListener(tauler);
            JLPausar.setText("Pausar");
            
        } else{
            
            tauler.removeMouseListener(tauler);
            JLPausar.setText("Reanudar");
            
        }

        pausado = !pausado;
            
    }
    
    private void JLCancelarMouseClicked(MouseEvent evt) {
        
        if(true){ //confirm
            
            this.setSize(500,300);
            cl.show(contenedor, "bienvenida");
        }
        
    }
    
    private void JLTerminarMouseClicked(MouseEvent evt) {
        closeFrame();
    }
    
    private void JLJugarMouseClicked(MouseEvent evt) {
        
        //demanar noms
        tauler.buida();
        tauler.repaint();
        this.setSize(tauler.getPreferredSize());
        cl.show(contenedor, "juego");
        pausado = false;
        turno = true;
        
    }
    
    /**
     * Code from stackoverflow.
     */
    public void closeFrame() {
        // this will make sure WindowListener.windowClosing() et al. will be called.
        WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);

        // this will hide and dispose the frame, so that the application quits by
        // itself if there is nothing else around. 
        setVisible(false);
        dispose();
        // if you have other similar frames around, you should dispose them, too.

        // finally, call this to really exit. 
        // i/o libraries such as WiiRemoteJ need this. 
        // also, this is what swing does for JFrame.EXIT_ON_CLOSE
        System.exit(0); 
    }
    
//    public void guanyat(){
//        cl.show(contenedor, "puntuaciones");
//    }

    public static void main(String[] args) {
        Conecta5 c5 = new Conecta5();
        c5.setVisible(true);
    }
    /**/
}
