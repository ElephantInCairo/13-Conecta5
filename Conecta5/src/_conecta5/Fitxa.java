/** Classe Pesa - José Sánchez Moreno - v.21.06.14.
 * 
 * Defineix les peces per a un joc de dos jugadors
 *
 * El constructor llegeix el fitxer amb la imatge
 *
 * El mètode paintComponent pinta la imatge a la posició indicada
 */

package _conecta5;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


/**
 *
 * @author jsm432 
 *  codi en base a la classe per miquelmascaro
 */

public class Fitxa {

    private boolean esJ1;
    private BufferedImage img;
    
    public static final Fitxa J1 = new Fitxa( true, "fitxes/j1.png");
    public static final Fitxa J2 = new Fitxa(false, "fitxes/j2.png");
    
    //constructor---------------------------------------------------------------
    private Fitxa(boolean b, String s) {
        try {
            esJ1 = b;
            img = ImageIO.read(new File(s));
        } catch (IOException e) {
        }
    }
    
    //getters-setters-----------------------------------------------------------
    public boolean esJ1(){
        return esJ1;
    }

    //altres--------------------------------------------------------------------
    public void paintComponent(Graphics g, float x, float y) {
        g.drawImage(img,(int) x + 10, (int) y + 10, null);
    }
    /**/
}
