/** Classe Posicio - José Sánchez Moreno - v.21.06.14.
 *
 * Aquesta classe maneja un punt 2D i els seus moviments
 * HORITZONTAL, VERTICAL, DIAGONAL1 (pendent amunt), DIAGONAL2 (pendent abaix)
 * 
 */

package _conecta5;


/**
 *
 * @author jsm432
 */

public class Posicio {
    
    private int x;
    private int y;
    
    //Constructors--------------------------------------------------------------
    /**
     * Constructors de Posició.
     *    Sense paràmetres crea una instància a (0,0)
     * @param a
     * @param b 
     */
    public Posicio(int a, int b){
        this.x = a;
        this.y = b;
    }
    /**
     * Constructor per defecte
     */
    public Posicio(){
        this(0,0);
    }
    
    /**
     * Constructor "Clonador"
     * @param p
     */
    public Posicio(Posicio p){
        this.x = p.getX();
        this.y = p.getY();
    }
    
    //getters i setters---------------------------------------------------------
    /**
     * Retorna el valor de la coordenada X
     * @return 
     */
    public int getX(){
        return x;
    }
    
    /**
     * Retorna el valor de la coordenada Y
     * @return 
     */
    public int getY(){
        return y;
    }
        
    @Override
    public String toString(){
        return "("+x+", "+y+")";
    }
    
    
    //moviment de caselles------------------------------------------------------
    
    /**
     * Enumerat que contempla les direccions en les que se revisarà si ha guanyat.
     */
    public enum Direccio {
        HORITZONTAL (0, 1), 
        VERTICAL    (1, 0),
        DIAGONAL1   (1, 1), 
        DIAGONAL2  (1, -1);
        
        private int x;
        private int y;

        Direccio(int a, int b){
            x = a;
            y = b;
        }
    }
    
    /**
     * Avança la posicó actual en la direcció indicada
     * @param d   Direcció en la que se moura de posició
     */
    protected void seguent(Direccio d){
        x += d.x;
        y += d.y;
    }
    
    /**
     * Retrocedeix la posicó actual en la direcció indicada
     * @param d   Direcció en la que se moura de posició
     */
    protected void anterior(Direccio d){
        x -= d.x;
        y -= d.y;
    }
    /**/
}
