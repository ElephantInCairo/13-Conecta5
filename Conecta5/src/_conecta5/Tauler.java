/** Clase Tauler - José Sánchez Moreno.
 *
 * El constructor defineix totes les caselles com a rectangles d'un color
 * determinat inicialitzant-les buides
 *
 * El mètode paintComponent recorr el tauler pintant les caselles
 *
 * El mètode getPreferredSize retorna el tamant del tauler
 *
 * El mètode posa col·loca una peça a la posició que indiquen els dos enters,
 * les peces són les constants definides per la classe Pesa
 */

package _conecta5;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;


/**
 *
 * @author jsm432 
 *  codi en base a la classe per miquelmascaro
 */

public class Tauler extends JPanel implements MouseListener {

    private static final int DIMENSIO = 15;
    private static final int MAXIM = 600;
    private static final int COSTAT = MAXIM / DIMENSIO;
    private final Casella t[][];

    //constructor---------------------------------------------------------------
    /**
     * Constructor. Crea un array de caselles, inicialitza les caselles
     */
    public Tauler() {
        
        t = new Casella[DIMENSIO][DIMENSIO];
        
        for (int i = 0, y = 0; DIMENSIO > i; i++, y += COSTAT) {
            
            for (int j = 0, x = 0; DIMENSIO > j; j++, x += COSTAT) {
                
                Rectangle2D.Float r =
                        new Rectangle2D.Float(x, y, COSTAT, COSTAT);
                
                t[i][j] = new Casella(r);
                
            }
        }
    }

    //getters i setters---------------------------------------------------------
    /**
     * Coloca la fitxa a la posició indicada. (Tauler)
     *      si hi havia una fitxa no la canvia i retorna fals
     * @param f    Fitxa
     * @param p    Posicio
     * @return     boolean
     */
    public boolean Posa(Fitxa f, Posicio p) {
        
        if(t[p.getX()][p.getY()].esPlena())
            return false;
        
        t[p.getX()][p.getY()].setFitxa(f);
        return true;
    }
    
    /**
     * Retorna la casella indicada a la posició p.
     * @param p    Posicio
     * @return     Casella
     */
    public Casella getCasella(Posicio p){
        
        return t[p.getX()][p.getY()];
    }
    
    /**
     * Conta les fitxes que hi ha colocades actualment al tauler.
     * @return   enter(numero)
     */
    public int fitxesEnJoc(){
        
        int n = 0;
        
        for (int i = 0; i < DIMENSIO; i++) {
            for (int j = 0; j < DIMENSIO; j++) {
                if(t[i][j].esPlena())
                    t[i][j].buida();
            }
        }
        
        return n;
    }
    
    /**
     * Et diu si la casella indicada es part d'una alineació guanyadora.
     * Actualment hi ha d'haver una fitxa perque funcioni l'algoritme
     * @param p
     * @return 
     */
    public boolean comprova(Posicio p){
        Posicio temp = new Posicio(p);
        
        //de quin jugador es la casella actual
        Casella c = getCasella(p);
        boolean j = c.getFitxa().esJ1(); //si esta buida fa feina ho intenta amb null!
        
        boolean cond;
        
        //comprova per a cada direccio
        for(Posicio.Direccio d : Posicio.Direccio.values() ){         
//            System.out.println("\n" +p+ " -> "+d);
            
            //intenta fer-te totes les posicions enrera que siguin fitxes de "j"
            try{
                
                //mou-te lo més enrera posible sempre que sigui la fitxa del mateix jugador
                //el bucle acaba quan troba la primera diferent
                do{
                    //actualitza casella actual --throws
                    p.anterior(d);
                    c = getCasella(p);       
                    //System.out.println(p+" retro");
                    
                    //comprova si es del mateix jugador
                    cond = c.esPlena()? (c.getFitxa().esJ1() == j) : false;
                } while(cond);
                
            } catch(Exception e){
                //System.out.println("fora tauler"+ d);
                //Excepció fora del tauler, no passa res,
                // segueix pero ara avançant i contant
            }
            
            //contador desde la primera diferent
            int seguides = 0;
//            System.out.println(p+" stateChange");

            try{
                //la fitxa és del mateix jugador?
                do{

                    //fe enrera fins que no ho sigui
                    //desde sa primera posició que es diferent...
                    seguides++;//antes de que pugui sortir per excep
                    //actualitza casella actual --throws
                    p.seguent(d);
                    c = getCasella(p);
                    
                    //System.out.println(p+" avan");
                    cond = c.esPlena()? (c.getFitxa().esJ1() == j) : false;
                    
                } while(cond);
                
            } catch(Exception e){
                //Excepció fora del tauler, no passa res,
                // segueix pero ara avançant i contant
            }
            
            //reinicia la posició per comprovar-ho amb la seguent direcció
            p = new Posicio(temp);
           
            //la darrera volta s'ha fet fins que ha trobat la primera diferent així que la descontam
            if(--seguides >= 5) return true;
        }
        
        //System.out.println("\n\n\n\n\n");
        return false;
    }
    
    public int buida(){
        
        int n = 0;
        
        for (int i = 0; i < DIMENSIO; i++) {
            for (int j = 0; j < DIMENSIO; j++) {
                if(t[i][j].esPlena())
                    n++;
            }
        }
        
        return n;
    }
    
    //JPanel--------------------------------------------------------------------
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(MAXIM, MAXIM+50);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        for (int i = 0; i < DIMENSIO; i++) {
            for (int j = 0; j < DIMENSIO; j++) {                
                t[i][j].paintComponent(g);
            }
        }
    }
    

    //MouseListener-------------------------------------------------------------
    public void mousePressed(MouseEvent e) {
        
        //obten la posicio de la casella
        int j = e.getX() / COSTAT;
        int i = e.getY() / COSTAT;
        
        Fitxa f = Conecta5.turno ? Fitxa.J1 : Fitxa.J2;

        //mira si esta plena
        if( !t[i][j].esPlena() ){ //--buida
            t[i][j].setFitxa(f);
            Conecta5.turno = !Conecta5.turno;
            this.repaint();
        } //--buida/plena

        if( comprova(new Posicio(i, j)) )
            //Ha guanyat el jugador !turno
            System.out.print("Guanyat");


    }

    public void mouseReleased(MouseEvent e) { }

    public void mouseEntered(MouseEvent e) { }

    public void mouseExited(MouseEvent e) { }

    public void mouseClicked(MouseEvent e) { }
    /**/
}